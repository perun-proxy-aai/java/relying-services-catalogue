package cz.muni.ics.serviceslist.data.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum RelyingServiceProtocol {

    OIDC("OIDC"),
    SAML("SAML"),
    UNKNOWN("UNKNOWN");

    private final String value;

    private static final Map<String, RelyingServiceProtocol> lookup = new HashMap<>();

    static {
        for (RelyingServiceProtocol protocol : EnumSet.allOf(RelyingServiceProtocol.class)) {
            lookup.put(protocol.value, protocol);
        }
    }

    public static RelyingServiceProtocol resolve(String value) {
        return lookup.getOrDefault(value, UNKNOWN);
    }

}
