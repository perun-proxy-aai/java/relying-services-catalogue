package cz.muni.ics.serviceslist.data;

import cz.muni.ics.serviceslist.data.model.SinglePropertyAggregation;
import cz.muni.ics.serviceslist.data.model.RelyingServiceDTO;
import cz.muni.ics.serviceslist.data.enums.RelyingServiceEnvironment;
import java.util.List;

import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RelyingServicesRepository extends MongoRepository<RelyingServiceDTO, Long> {

    List<RelyingServiceDTO> findAllByEnvironment(RelyingServiceEnvironment environment);

    @Aggregation(pipeline = {
            "{ '$group': { '_id' : '$jurisdiction', count : {$sum : 1} } }",
            "{ '$sort': { 'count': -1} }"
    })
    AggregationResults<SinglePropertyAggregation> getCountsByJurisdiction();

    @Aggregation(pipeline = {
            "{ '$group': { '_id' : '$environment', count : {$sum : 1} } }",
            "{ '$sort': { 'count': -1} }"
    })
    AggregationResults<SinglePropertyAggregation> getCountsByEnvironment();

    @Aggregation(pipeline = {
            "{ '$group': { '_id' : '$protocol', count : {$sum : 1} } }",
            "{ '$sort': { 'count': -1} }"
    })
    AggregationResults<SinglePropertyAggregation> getCountsByProtocol();

}
