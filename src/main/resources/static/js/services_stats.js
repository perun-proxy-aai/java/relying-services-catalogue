new Chart(document.getElementById("services_by_protocol"), {
  type: "bar",
  data: {
    labels: dataByProtocol.map((row) => row.protocol),
    datasets: [
      {
        label: "By protocol",
        data: dataByProtocol.map((row) => row.count),
      },
    ],
  },
});

new Chart(document.getElementById("services_by_env"), {
  type: "bar",
  data: {
    labels: dataByEnv.map((row) => row.environment),
    datasets: [
      {
        label: "By environment",
        data: dataByEnv.map((row) => row.count),
      },
    ],
  },
});

new Chart(document.getElementById("services_by_jurisdiction"), {
  type: "pie",
  data: {
    labels: dataByJurisdiction.map((row) => row.jurisdiction),
    datasets: [
      {
        label: "By jurisdiction",
        data: dataByJurisdiction.map((row) => row.count),
      },
    ],
  },
});
