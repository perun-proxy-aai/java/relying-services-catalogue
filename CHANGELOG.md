## [1.1.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.12...v1.1.13) (2023-11-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.18 ([c3c1945](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/c3c1945334e325360c9892820b92bdd41678ad9d))

## [1.1.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.11...v1.1.12) (2023-10-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.17 ([a3eb4d6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/a3eb4d69e20b466620346c2a283ec197b8b10dff))

## [1.1.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.10...v1.1.11) (2023-09-24)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.16 ([66d0a80](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/66d0a80fce3b2c9989267718f60bbb3e78480ee2))

## [1.1.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.9...v1.1.10) (2023-09-18)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.3.2 ([6f74660](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/6f74660e07168ff42ad1c0352415a413abf20735))

## [1.1.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.8...v1.1.9) (2023-09-01)


### Bug Fixes

* **deps:** update dependency org.webjars:jquery to v3.7.1 ([061ad43](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/061ad4385f77076dac30696d464219d8c8b9ae3f))

## [1.1.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.7...v1.1.8) (2023-08-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.15 ([d0d6319](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/d0d6319807c1cd8e68774ca98b52286edbed534b))

## [1.1.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.6...v1.1.7) (2023-08-17)


### Bug Fixes

* **deps:** update dependency org.webjars:font-awesome to v6.4.2 ([f3511d0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f3511d07dc7be5aaba89db19d05b4bcee426cff5))

## [1.1.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.5...v1.1.6) (2023-08-03)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.3.1 ([90cd8aa](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/90cd8aa46c6b606124d910cfb768828100950fbd))

## [1.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.4...v1.1.5) (2023-07-31)


### Bug Fixes

* **deps:** update dependency org.webjars:jquery to v3.7.0 ([ea63d2d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/ea63d2d51916445beb7bae7d339c9942516ec1f6))

## [1.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.3...v1.1.4) (2023-07-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.14 ([1f6b678](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/1f6b6785c59232272078d33816294a7b3b887266))

## [1.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.2...v1.1.3) (2023-07-19)


### Bug Fixes

* **deps:** update datatables.version to v1.13.5 ([180ffd8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/180ffd84d08068702479feae2b17fa3b0880f9c4))

## [1.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.1...v1.1.2) (2023-06-27)


### Bug Fixes

* 🐛 Fix protocol form item ([4fb77dd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/4fb77dded2ceac967958eea5f1482a3fb50e67b5))
* 🐛 fix Protocol resolution when mapping to DTO ([5c9bcb8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/5c9bcb840f190338368804363fb6c455ddd736f8))

## [1.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.1.0...v1.1.1) (2023-06-27)


### Bug Fixes

* 🐛 Fix loading wrong translation for protocol in detail ([5e56df3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/5e56df3f1b3a81a70890d7abf957ff10afc02423))

# [1.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.30...v1.1.0) (2023-06-27)


### Features

* 🎸 Add statistics page ([98b361d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/98b361de9fef9be6dca554ddb4ccbc95a48594ca))

## [1.0.30](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.29...v1.0.30) (2023-06-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.13 ([6450bcb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/6450bcb1dd2773273d64afd483f72c0959c3752c))

## [1.0.29](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.28...v1.0.29) (2023-06-20)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.3.0 ([f671245](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f671245af72a0e24e4d4f532030537eb0ca2f917))

## [1.0.28](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.27...v1.0.28) (2023-06-19)


### Bug Fixes

* **deps:** update dependency org.webjars:datatables to v1.13.4 ([f23af0b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f23af0bc5c0e0d2e3de0fb15133614a4fd5bdd89))

## [1.0.27](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.26...v1.0.27) (2023-05-21)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.12 ([1ad3487](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/1ad3487c5afce9507ddad480ff50fbe877e0299e))

## [1.0.26](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.25...v1.0.26) (2023-04-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.11 ([f5c0275](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f5c0275f2a98b4a6e06c2ac71a0270c60694c4ee))

## [1.0.25](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.24...v1.0.25) (2023-04-12)


### Bug Fixes

* 🐛 Fix generating links for details of services ([f4b48c9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f4b48c99d7ee446343e812ccf0a723bb2850e1f6))

## [1.0.24](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.23...v1.0.24) (2023-04-11)


### Bug Fixes

* **deps:** update dependency org.webjars:font-awesome to v6.4.0 ([d3f8662](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/d3f866282923eb6ce81ae1472eb85b6a3d98cda3))

## [1.0.23](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.22...v1.0.23) (2023-03-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.10 ([5dc992a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/5dc992a51f23451dc12f15b273a90e17e4779d3d))

## [1.0.22](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.21...v1.0.22) (2023-03-17)


### Bug Fixes

* **deps:** update dependency org.webjars:jquery to v3.6.4 ([4db88e0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/4db88e0ba202c1afcaf197f14b4e900c94538d35))

## [1.0.21](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.20...v1.0.21) (2023-02-26)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.9 ([6cba1f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/6cba1f127f4b98459a442d7c8f9550a061c5837f))

## [1.0.20](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.19...v1.0.20) (2023-02-17)


### Bug Fixes

* **deps:** update dependency org.webjars:datatables to v1.13.2 ([e635250](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/e635250ce4d6c301ab4bc5d86811160fe579341d))

## [1.0.19](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.18...v1.0.19) (2023-01-22)


### Bug Fixes

* **deps:** update dependency org.webjars:font-awesome to v6.2.1 ([74a5bab](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/74a5babdd33868dec192f25db9e4012a60002b75))

## [1.0.18](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.17...v1.0.18) (2023-01-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.8 ([e2e5702](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/e2e5702e1016386a7b3c7672604efa6e32dec60f))

## [1.0.17](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.16...v1.0.17) (2023-01-08)


### Bug Fixes

* **deps:** update dependency org.webjars:jquery to v3.6.3 ([e63effe](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/e63effeae0289ef21f76a3ad1a75adb448234ee4))

## [1.0.16](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.15...v1.0.16) (2023-01-04)


### Bug Fixes

* 🐛 Fix logback configuration ([5841846](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/58418469a71f9b296009d6679217b96a62a958ab))

## [1.0.15](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.14...v1.0.15) (2023-01-04)


### Bug Fixes

* 🐛 logging configuration in XML file ([353560b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/353560b6c828cb576bb9008553da23558a3a448e))

## [1.0.14](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.13...v1.0.14) (2022-12-25)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.7 ([e964259](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/e96425954cc7192828e1d7e53786edb0ec4dfa3d))

## [1.0.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.12...v1.0.13) (2022-12-02)


### Bug Fixes

* **deps:** update dependency org.webjars:datatables to v1.13.1 ([4ba1ed6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/4ba1ed62d966eb44e6c8115f19903658956f1c42))

## [1.0.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.11...v1.0.12) (2022-11-30)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.2.3 ([f87ae5a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/f87ae5aa6d9da724660845eec9610cc85107c1fa))

## [1.0.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.10...v1.0.11) (2022-11-27)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.6 ([179f11e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/179f11eb8f7a082fabb58b1f8c5a50fc8ea1095e))

## [1.0.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.9...v1.0.10) (2022-10-23)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.5 ([b0f4228](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/b0f42286fe381b56d0f1744c448a39d6f8d6f2e3))

## [1.0.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.8...v1.0.9) (2022-10-07)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.2.2 ([cbb2b66](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/cbb2b666e29b5900c43a791bacbd9d0bc813fdc4))

## [1.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.7...v1.0.8) (2022-10-06)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([4f514b6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/4f514b6480e5d730513ef49b2aa5e4545a158882))
* **deps:** update dependency org.webjars:font-awesome to v6.2.0 ([9bb4bc5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/9bb4bc5db57dcffdd8d5e849e8838c27bf7fe2b1))

## [1.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.6...v1.0.7) (2022-10-04)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.4 ([196e4d1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/196e4d13aa9a6c659214c957979c90f95d71e59a))

## [1.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/compare/v1.0.5...v1.0.6) (2022-09-28)


### Bug Fixes

* first release in GitLab ([bf25878](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/relying-services-catalogue/commit/bf2587843b8a9a511e137893d4f2229c85538fa0))

## [1.0.5](https://github.com/CESNET/relying-services-catalogue/compare/v1.0.4...v1.0.5) (2022-09-13)


### Bug Fixes

* **deps:** update dependency org.webjars:jquery to v3.6.1 ([698e766](https://github.com/CESNET/relying-services-catalogue/commit/698e766faa8d794b455cd2897194a8d844956a0f))

## [1.0.4](https://github.com/CESNET/relying-services-catalogue/compare/v1.0.3...v1.0.4) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.springframework.boot:spring-boot-starter-parent to v2.7.3 ([9616270](https://github.com/CESNET/relying-services-catalogue/commit/9616270626174494b4b9333dd0acc3db346fd71d))

## [1.0.3](https://github.com/CESNET/relying-services-catalogue/compare/v1.0.2...v1.0.3) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.webjars:datatables to v1.12.1 ([f88186a](https://github.com/CESNET/relying-services-catalogue/commit/f88186a9531b07e098e2cb88e0bea6cb993eb320))

## [1.0.2](https://github.com/CESNET/relying-services-catalogue/compare/v1.0.1...v1.0.2) (2022-08-08)


### Bug Fixes

* **deps:** update dependency org.webjars:bootstrap to v5.2.0 ([15bf733](https://github.com/CESNET/relying-services-catalogue/commit/15bf733246e80276b07d4f0fb9b489bf2040c6b1))

## [1.0.1](https://github.com/CESNET/relying-services-catalogue/compare/v1.0.0...v1.0.1) (2022-08-03)


### Bug Fixes

* **deps:** update dependency org.webjars:font-awesome to v6.1.2 ([f1badc6](https://github.com/CESNET/relying-services-catalogue/commit/f1badc6ac57f81f1183c5ced70a87239566651ac))

# 1.0.0 (2022-06-15)

* Initial implementation
